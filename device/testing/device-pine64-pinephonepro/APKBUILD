# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Martijn Braam <martijn@brixit.nl>
pkgname=device-pine64-pinephonepro
pkgdesc="PINE64 PinePhone Pro"
pkgver=1.3
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="
	alsa-ucm-conf>=1.2.6.2
	eg25-manager>=0.4.2
	iw
	linux-pine64-pinephonepro
	mesa-dri-gallium
	postmarketos-base
	u-boot-pine64-pinephonepro
	fwupd
	fwupd-plugin-modem_manager
	fwupd-plugin-fastboot
	"
makedepends="devicepkg-dev"
subpackages="$pkgname-nonfree-firmware:nonfree_firmware"
install="
	$pkgname.post-install
	$pkgname.post-upgrade
	"
source="
	deviceinfo
	extlinux.conf
	ucm/HiFi.conf
	ucm/PinePhonePro.conf
	ucm/VoiceCall.conf
	"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
	install -D -m644 "$srcdir"/extlinux.conf \
		"$pkgdir"/boot/extlinux/extlinux.conf

	install -Dm644 -t "$pkgdir"/usr/share/alsa/ucm2/conf.d/simple-card \
		"$srcdir"/PinePhonePro.conf \
		"$srcdir"/HiFi.conf \
		"$srcdir"/VoiceCall.conf
}

nonfree_firmware() {
	pkgdesc="Wifi, Bluetooth and video-out firmware"
	depends="
		firmware-pine64-pinebookpro
		linux-firmware-rtlwifi
		linux-firmware-rtl_bt
		firmware-pine64-rtl8723bt
		firmware-pine64-ov5640
		"
	mkdir "$subpkgdir"
}

sha512sums="
f42c2c13bf8363a2427ad501e922d0e1fb813a750c3df882f4b394a2e40efbacc9b9deb7b25cd253a2e1cd6448ad0541bfe2f6326f179c329b28f518e4915850  deviceinfo
b116e5286880c2b8774fc065e4312d2c89a06003c85f332519b827b42b4e0630a1c47b69f75d56c5c91469eb810f18c6de21cf52234e59c67478c4c1731d347e  extlinux.conf
ac22c856af81d00aa6a349b68cf9e8645bf38277d9aafd07f1f46f5f932f48d37b9dfdcc8772fff4027f914c9b4c3b4a11c51bd9f2aa1abbc53abd3f54adb818  HiFi.conf
c57dae885c9a5f366f18b38a3ce3e21627baaf014724537eced9e8d6ac3ca61ade42b9fcf84db350b1e64742760e8cf4fe10639d41052387927238e85c3c4769  PinePhonePro.conf
e978876bda8874e30df75c80554ccbbc0dd202c852ecae0b5c1a0d845402a630962afc2691c6f7d5f478fb0e4be045af4ef62ad0b1ce77f62fe2f155dc0a9cff  VoiceCall.conf
"
